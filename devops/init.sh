#!/usr/bin/env bash

composer install --no-interaction
php artisan key:generate
php artisan config:cache
php artisan make:auth
php-fpm
